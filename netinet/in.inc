//the Pascal translation of libctru headers files for the nintendo 3ds platform
//
// Copyright (c) 2016 Kenny D. Lee
// all rights reserved.
//

//sock in;

  const
    INADDR_LOOPBACK = $7f000001;    
    INADDR_ANY = $00000000;    
    INADDR_BROADCAST = $FFFFFFFF;    
    INADDR_NONE = $FFFFFFFF;    
    INET_ADDRSTRLEN = 16;    
  {
   * Protocols (See RFC 1700 and the IANA)
    }
  { dummy for IP  }
    IPPROTO_IP = 0;    
  { user datagram protocol  }
    IPPROTO_UDP = 17;    
  { tcp  }
    IPPROTO_TCP = 6;    
    IP_TOS = 7;    
    IP_TTL = 8;    
    IP_MULTICAST_LOOP = 9;    
    IP_MULTICAST_TTL = 10;    
    IP_ADD_MEMBERSHIP = 11;    
    IP_DROP_MEMBERSHIP = 12;    

  type
    in_port_t = uint16_t;

    in_addr_t = uint32_t;
    in_addr = record
        s_addr : in_addr_t;
      end;

    sockaddr_in = record
        sin_family : sa_family_t;
        sin_port : in_port_t;
        sin_addr : in_addr;
        sin_zero : array[0..7] of cuchar;
      end;

  { Request struct for multicast socket ops  }
  { IP multicast address of group  }
  { local IP address of interface  }
    ip_mreq = record
        imr_multiaddr : in_addr;
        imr_interface : in_addr;
      end;

