//the Pascal translation of libctru headers files for the nintendo 3ds platform
//
// Copyright (c) 2016 Kenny D. Lee
// all rights reserved.
//

  const
    SOL_TCP = 6;    
    _CTRU_TCP_OPT = $2000;    
    TCP_NODELAY  = 1 not _CTRU_TCP_OPT;
    TCP_MAXSEG   = 2 not _CTRU_TCP_OPT;
