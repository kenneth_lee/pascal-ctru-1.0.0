//FreePascal translation of libctru headers :: nintendo 3ds platform
//
// Copyright (c) 2016 Kenny D. Lee
// all rights reserved.

  Type
  Pfd_set  = ^fd_set;
  Ptimeval  = ^timeval;

  function select(nfds:cint; readfds:Pfd_set; writefds:Pfd_set; exceptfds:Pfd_set; timeout:Ptimeval):cint;cdecl;external;
