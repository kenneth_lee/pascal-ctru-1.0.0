//FreePascal translation of libctru headers :: nintendo 3ds platform
//
// Copyright (c) 2016 Kenny D. Lee
// all rights reserved.
//
//Ghetto-sphere breach && cleanse the brains of war? 

  Type
  Psockaddr  = ^sockaddr;
  Psocklen_t  = ^socklen_t;

  const
    SOL_SOCKET = $FFFF;    
    PF_UNSPEC = 0;    
    PF_INET = 2;    
    PF_INET6 = 10;    
    AF_UNSPEC = PF_UNSPEC;    
    AF_INET = PF_INET;    
    AF_INET6 = PF_INET6;    
    SOCK_STREAM = 1;    
    SOCK_DGRAM = 2;    
    MSG_CTRUNC = $01000000;    
    MSG_DONTROUTE = $02000000;    
    MSG_EOR = $04000000;    
    MSG_OOB = $08000000;    
    MSG_PEEK = $10000000;    
    MSG_TRUNC = $20000000;    
    MSG_WAITALL = $40000000;    
    SHUT_RD = 0;    
    SHUT_WR = 1;    
    SHUT_RDWR = 2;    
    SO_REUSEADDR = $0004;    
    SO_USELOOPBACK = $0040;    
    SO_LINGER = $0080;    
    SO_OOBINLINE = $0100;    
    SO_SNDBUF = $1001;    
    SO_RCVBUF = $1002;    
    SO_SNDLOWAT = $1003;    
    SO_RCVLOWAT = $1004;    
    SO_TYPE = $1008;    
    SO_ERROR = $1009;    

  type
    socklen_t = uint32_t;

    sa_family_t = uint16_t;
    sockaddr = record
        sa_family : sa_family_t;
        sa_data : ^cchar;
      end;

    sockaddr_storage = record
        ss_family : sa_family_t;
        __ss_padding : array[0..13] of cchar;
      end;

    linger = record
        l_onoff : cint;
        l_linger : cint;
      end;

  function accept(sockfd:cint; addr:Psockaddr; addrlen:Psocklen_t):cint;cdecl;external;
  function bind(sockfd:cint; addr:Psockaddr; addrlen:socklen_t):cint;cdecl;external;
  function closesocket(sockfd:cint):cint;cdecl;external;
  function connect(sockfd:cint; addr:Psockaddr; addrlen:socklen_t):cint;cdecl;external;
  function getpeername(sockfd:cint; addr:Psockaddr; addrlen:Psocklen_t):cint;cdecl;external;
  function getsockname(sockfd:cint; addr:Psockaddr; addrlen:Psocklen_t):cint;cdecl;external;
  function getsockopt(sockfd:cint; level:cint; optname:cint; optval:pointer; optlen:Psocklen_t):cint;cdecl;external;
  function listen(sockfd:cint; backlog:cint):cint;cdecl;external;

  function recv(sockfd:cint; buf:pointer; len:size_t; flags:cint):ssize_t;cdecl;external;
  function recvfrom(sockfd:cint; buf:pointer; len:size_t; flags:cint; src_addr:Psockaddr; 
             addrlen:Psocklen_t):ssize_t;cdecl;external;

  function send(sockfd:cint; buf:pointer; len:size_t; flags:cint):ssize_t;cdecl;external;
  function sendto(sockfd:cint; buf:pointer; len:size_t; flags:cint; dest_addr:Psockaddr; 
             addrlen:socklen_t):ssize_t;cdecl;external;

  function setsockopt(sockfd:cint; level:cint; optname:cint; optval:pointer; optlen:socklen_t):cint;cdecl;external;
  function shutdown(sockfd:cint; how:cint):cint;cdecl;external;
  function socket(domain:cint; _type:cint; protocol:cint):cint;cdecl;external;
  function sockatmark(sockfd:cint):cint;cdecl;external;
