//FreePascal translation of libctru headers :: nintendo 3ds platform
//
// Copyright (c) 2016 Kenny D. Lee
// all rights reserved.

  const
    FIONBIO = 1;    

  function ioctl(fd:cint; request:cint; args:array of const):cint;cdecl;external;
  function ioctl(fd:cint; request:cint):cint;cdecl;external;
