//the Pascal translation of libctru 1.0.0 headers files for the nintendo 3ds platform
//
// Copyright (c) 2016 Kenny D. Lee
// all rights reserved
//

// vram 
  function vramAlloc(size:size_t):pointer;cdecl;external;
  function vramMemAlign(size:size_t; alignment:size_t):pointer;cdecl;external;
  function vramRealloc(mem:pointer; size:size_t):pointer;cdecl;external;
  procedure vramFree(mem:pointer);cdecl;external;
  function vramSpaceFree:u32;cdecl;external;

//linear
  function linearAlloc(size:size_t):pointer;cdecl;external;
  function linearMemAlign(size:size_t; alignment:size_t):pointer;cdecl;external;
  function linearRealloc(mem:pointer; size:size_t):pointer;cdecl;external;
  procedure linearFree(mem:pointer);cdecl;external;
  function linearSpaceFree:u32;cdecl;external;

//mappable
  function mappableAlloc(size:size_t):pointer;cdecl;external;
  procedure mappableFree(mem:pointer);cdecl;external;
  function mappableSpaceFree:u32;cdecl;external;

//reffer to linear, vram & mappable intructions
//https://github.com/smealum/ctrulib/tree/master/libctru/include/3ds/allocator
