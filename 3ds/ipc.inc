//the Pascal translation of libctru headers files for the nintendo 3ds platform
//
// Copyright (c) 2015 Kenny D. Lee
// all rights reserved
//

{$ifdef 3dsintf}
  type
    IPC_BufferRights = (IPC_BUFFER_R := BIT(1),IPC_BUFFER_W := BIT(2),
      IPC_BUFFER_RW := IPC_BUFFER_R or IPC_BUFFER_W);

  function IPC_MakeHeader(command_id:u16; normal_params:u32; translate_params:u32):u32;cdecl;
  function IPC_Desc_SharedHandles(number:u32):u32;cdecl;
  function IPC_Desc_MoveHandles(number:u32):u32;cdecl;
  function IPC_Desc_CurProcessHandle:u32;cdecl;
  function IPC_Desc_StaticBuffer(size:size_t; buffer_id:cunsigned):u32;cdecl;
  function IPC_Desc_PXIBuffer(size:size_t; buffer_id:cunsigned; is_read_only:bool):u32;cdecl;
  function IPC_Desc_Buffer(size:size_t; rights:IPC_BufferRights):u32;cdecl;
{$endif 3dsintf}


{$ifdef 3dsimpl}
function IPC_MakeHeader(command_id:u16; normal_params:u32; translate_params:u32):u32; cdecl; 
  begin
    IPC_MakeHeader:=((command_id shl 16) or ((normal_params and $3F) shl 6)) or ((translate_params and $3F) shl 0);
  end;
  
function IPC_Desc_Buffer(size:size_t; rights:IPC_BufferRights):u32;cdecl;
  begin
     IPC_Desc_Buffer:= (size shl 4) or 0x8 or rights
  end;
  
function IPC_Desc_StaticBuffer(size:size_t; buffer_id:cunsigned):u32;cdecl;
  begin
     IPC_Desc_StaticBuffer:=  (size shl 14) or ((buffer_id and 0xF) shl 10) or 0x2;
  end;

function IPC_Desc_CurProcessHandle:u32;cdecl;
  begin
     IPC_Desc_CurProcessHandle:=  0x20;
  end;
  
function IPC_Desc_MoveHandles(number:u32):u32;cdecl;
  begin
     IPC_Desc_MoveHandles:= ((u32)(number - 1) shl 26) or 0x10
  end;
  
    
function IPC_Desc_SharedHandles(number:u32):u32;cdecl;
  begin
     IPC_Desc_SharedHandles:= ((u32)(number - 1) shl 26);
  end;
    
function IPC_Desc_PXIBuffer(size:size_t; buffer_id:cunsigned; is_read_only:bool):u32;cdecl;
var 
 PXIBuffer: u8;
  begin
     PXIBuffer := 0x4;
     if(is_read_only) then PXIBuffer := 0x6;
     IPC_Desc_PXIBuffer:= (size shl 8) | ((buffer_id and 0xF) shl 4) or PXIBuffer;
  end;
  
{$endif 3dsimpl}
