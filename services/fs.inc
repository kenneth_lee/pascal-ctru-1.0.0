//the Pascal translation of libctru 1.0.0 headers files for the nintendo 3ds platform
//
// Copyright (c) 2016 Kenny D. Lee
// all rights reserved
//

{ 
unit fs;
interface

uses
  ctypes;

    Type
    Pbool  = ^boolean;
     u8 = cuint8;
     pu8 = ^u8;
     u16 = cuint16;
     pu16 = ^u16;
     u32 = cuint32;
     pu32 = ^u32;
     u64 = cuint64;
     pu64 = ^u64;

     s8 = cint8;
     s16 = cint16;
     s32 = cint32;
     ps32 = ^s32;
     s64 = cint64;
     ps64 = ^s64;

     bool= boolean;

    Handle  = s32;
    PHandle  = ^Handle;
}
{$IFDEF FPC}
{$PACKRECORDS C}
{$ENDIF}


{$ifdef 3dsintf}

Const
        FS_OPEN_READ =   $00000000;
        FS_OPEN_WRITE =  $00000001;
        FS_OPEN_CREATE = $00000010;


     FS_WRITE_FLUSH = $00010001;     
     FS_WRITE_NOFLUSH = $00000000; 
     FS_WRITE_UPDATE_TIME = $10000000; 

    {/ Attribute flags. }

  {! Create attributes.  }
     FS_ATTRIBUTE_NONE =      $01000000;    // still needs defined ?
     FS_ATTRIBUTE_READONLY =  $00011000; 
     FS_ATTRIBUTE_ARCHIVE =   $00010000;  
     FS_ATTRIBUTE_HIDDEN =    $00001000;     
     FS_ATTRIBUTE_DIRECTORY = $00000000;  


    type
      FS_MediaType =  Longint;
      PFS_MediaType =  ^FS_MediaType;
      Const
        MEDIATYPE_NAND = 0;
        MEDIATYPE_SD = 1;
        MEDIATYPE_GAME_CARD = 2;

    type
      FS_ArchiveID =  Longint;
      Const
        ARCHIVE_ROMFS = $00000003;
        ARCHIVE_SAVEDATA = $00000004;
        ARCHIVE_EXTDATA = $00000006;
        ARCHIVE_SHARED_EXTDATA = $00000007;
        ARCHIVE_SYSTEM_SAVEDATA = $00000008;
        ARCHIVE_SDMC = $00000009;
        ARCHIVE_SDMC_WRITE_ONLY = $0000000A;
        ARCHIVE_BOSS_EXTDATA = $12345678;
        ARCHIVE_CARD_SPIFS = $12345679;
        ARCHIVE_EXTDATA_AND_BOSS_EXTDATA = $1234567B;
        ARCHIVE_SYSTEM_SAVEDATA2 = $1234567C;
        ARCHIVE_NAND_RW = $1234567D;
        ARCHIVE_NAND_RO = $1234567E;
        ARCHIVE_NAND_RO_WRITE_ACCESS = $1234567F;
        ARCHIVE_SAVEDATA_AND_CONTENT = $2345678A;
        ARCHIVE_SAVEDATA_AND_CONTENT2 = $2345678E;
        ARCHIVE_NAND_CTR_FS = $567890AB;
        ARCHIVE_TWL_PHOTO = $567890AC;
        ARCHIVE_NAND_TWL_FS = $567890AE;
        ARCHIVE_NAND_W_FS = $567890AF;
        ARCHIVE_GAMECARD_SAVEDATA = $567890B1;
        ARCHIVE_USER_SAVEDATA = $567890B2;
        ARCHIVE_DEMO_SAVEDATA = $567890B4;

    type
      FS_PathType =  Longint;
      PFS_PathType =  ^FS_PathType;
      Const
        PATH_INVALID = 0;
        PATH_EMPTY = 1;
        PATH_BINARY = 2;
        PATH_ASCII = 3;
        PATH_UTF16 = 4;

    {/ Secure value slot. }
    {/< SD application. }

    type
      FS_SecureValueSlot =  Longint;
      Const
        SECUREVALUE_SLOT_SD = $1000;

    {/ Card SPI baud rate. }
    {/< 512KHz. }
    {/< 1MHz. }
    {/< 2MHz. }
    {/< 4MHz. }
    {/< 8MHz. }
    {/< 16MHz. }

    type
      FS_CardSpiBaudRate =  Longint;
      Const
        BAUDRATE_512KHZ = 0;
        BAUDRATE_1MHZ = 1;
        BAUDRATE_2MHZ = 2;
        BAUDRATE_4MHZ = 3;
        BAUDRATE_8MHZ = 4;
        BAUDRATE_16MHZ = 5;

    {/ Card SPI bus mode. }
    {/< 1-bit. }
    {/< 4-bit. }

    type
      FS_CardSpiBusMode =  Longint;
      Const
        BUSMODE_1BIT = 0;
        BUSMODE_4BIT = 1;

    {/ Card SPI bus mode. }
    {/< Update. }
    {/< Manual. }
    {/< DLP child. }

    type
      FS_SpecialContentType =  Longint;
      Const
        SPECIALCONTENT_UPDATE = 1;
        SPECIALCONTENT_MANUAL = 2;
        SPECIALCONTENT_DLP_CHILD = 3;

    {/< CTR card. }
    {/< TWL card. }

    type
      FS_CardType =  Longint;
      PFS_CardType =  ^ FS_CardType;
      Const
        CARD_CTR = 0;
        CARD_TWL = 1;

    {/ FS control actions. }

    type
      FS_Action =  Longint;
      Const
        FS_ACTION_UNKNOWN = 0;

    {/ Archive control actions. }
    {/< Commits save data changes. No inputs/outputs. }
    {/< Retrieves a file's last-modified timestamp. In: "u16*, UTF-16 Path", Out: "u64, Time Stamp". }

    type
      FS_ArchiveAction =  Longint;
      PFS_ArchiveAction =  ^FS_ArchiveAction;
      Const
        ARCHIVE_ACTION_COMMIT_SAVE_DATA = 0;
        ARCHIVE_ACTION_GET_TIMESTAMP = 1;

    {/ Secure save control actions. }
    {/< Deletes a save's secure value. In: "u64, ((SecureValueSlot << 32) | (TitleUniqueId << 8) | TitleVariation)", Out: "u8, Value Existed" }
    {/< Formats a save. No inputs/outputs. }

    type
      FS_SecureSaveAction =  Longint;
      PFS_SecureSaveAction =  ^FS_SecureSaveAction;
      Const
        SECURESAVE_ACTION_DELETE = 0;
        SECURESAVE_ACTION_FORMAT = 1;

    {/ File control actions. }

    type
      FS_FileAction =  Longint;
      PFS_FileAction =  ^FS_FileAction;
      Const
        FILE_ACTION_UNKNOWN = 0;

    {/ Directory control actions. }

    type
      FS_DirectoryAction =  Longint;
      PFS_DirectoryAction =  ^FS_DirectoryAction;
      Const
        DIRECTORY_ACTION_UNKNOWN = 0;


    type
      FS_DirectoryEntry = record
          name : array[0..261] of u16;
          shortName : array[0..9] of cchar;
          shortExt : array[0..3] of cchar;
          valid : u8;
          reserved : u8;
          attributes : u32;
          fileSize : u64;
        end;
      PFS_DirectoryEntry =  ^FS_DirectoryEntry;

      FS_ArchiveResource = record
          sectorSize : u32;
          clusterSize : u32;
          totalClusters : u32;
          freeClusters : u32;
        end;
      PFS_ArchiveResource =  ^FS_ArchiveResource;

      FS_ProgramInfo = record
          programId : u64;
          flag0 : word;
          padding : array[0..6] of u8;
        end;
      PFS_ProgramInfo =  ^FS_ProgramInfo;

    const
      bm_FS_ProgramInfo_mediaType = $FF;
      bp_FS_ProgramInfo_mediaType = 0;

    function mediaType(var a : FS_ProgramInfo) : FS_MediaType;
    procedure set_mediaType(var a : FS_ProgramInfo; __mediaType : FS_MediaType);

    type
      FS_ProductInfo = record
          productCode : array[0..15] of cchar;
          companyCode : array[0..1] of cchar;
          remasterVersion : u16;
        end;
      PFS_ProductInfo =  ^FS_ProductInfo;

    {/ Integrity verification seed. }
    {/< AES-CBC MAC over a SHA256 hash, which hashes the first 0x110-bytes of the cleartext SEED. }
    {/< The "nand/private/movable.sed", encrypted with AES-CTR using the above MAC for the counter. }

      FS_IntegrityVerificationSeed = record
          aesCbcMac : array[0..15] of u8;
          movableSed : array[0..287] of u8;
        end;
      PFS_IntegrityVerificationSeed =  ^FS_IntegrityVerificationSeed;

       FS_ExtSaveDataInfo = record
          flag0 : word;
          unknown : u8;
          reserved1 : u16;
          saveId : u64;
          reserved2 : u32;
        end;
      pFS_ExtSaveDataInfo = ^FS_ExtSaveDataInfo;

    const
      bm_PACKED_mediaType = $FF;
      bp_PACKED_mediaType = 0;

    function mediaType(var a : FS_ExtSaveDataInfo) : FS_MediaType;
    procedure set_mediaType(var a : FS_ExtSaveDataInfo; __mediaType : FS_MediaType);

    type
      FS_SystemSaveDataInfo = record
          flag0 : word;
          unknown : u8;
          reserved : u16;
          saveId : u32;
        end;

    const
      bm_FS_SystemSaveDataInfo_mediaType = $FF;
      bp_FS_SystemSaveDataInfo_mediaType = 0;

    function mediaType(var a : FS_SystemSaveDataInfo) : FS_MediaType;
    procedure set_mediaType(var a : FS_SystemSaveDataInfo; __mediaType : FS_MediaType);

    type
      FS_DeviceMoveContext = record
          ivs : array[0..15] of u8;
          encryptParameter : array[0..15] of u8;
        end;

      PFS_DeviceMoveContext =  ^FS_DeviceMoveContext;

      FS_Path = record
          _type : FS_PathType;
          size : u32;
          data : pointer;
        end;

      FS_Archive = record
          id : u32;
          lowPath : FS_Path;
          handle : u64;
        end;
      PFS_Archive  = ^FS_Archive;

    function fsInit:s32;cdecl;external;
    procedure fsExit;cdecl;external;

    procedure fsUseSession(session:Handle; sdmc:bool);cdecl;external;

    procedure fsEndUseSession;cdecl;external;

    function fsMakePath(_type:FS_PathType; path:pointer):FS_Path;cdecl;external;

    function fsGetSessionHandle:PHandle;cdecl;external;

    function FSUSER_Control(action:FS_Action; input:pointer; inputSize:u32; output:pointer; outputSize:u32):s32;cdecl;external;

    function FSUSER_Initialize(session:Handle):s32;cdecl;external;

    function FSUSER_OpenFile(out:PHandle; archive:FS_Archive; path:FS_Path; openFlags:u32; attributes:u32):s32;cdecl;external;

    function FSUSER_OpenFileDirectly(out:PHandle; archive:FS_Archive; path:FS_Path; openFlags:u32; attributes:u32):s32;cdecl;external;

    function FSUSER_DeleteFile(archive:FS_Archive; path:FS_Path):s32;cdecl;external;

    function FSUSER_RenameFile(srcArchive:FS_Archive; srcPath:FS_Path; dstArchive:FS_Archive; dstPath:FS_Path):s32;cdecl;external;

    function FSUSER_DeleteDirectory(archive:FS_Archive; path:FS_Path):s32;cdecl;external;

    function FSUSER_DeleteDirectoryRecursively(archive:FS_Archive; path:FS_Path):s32;cdecl;external;

    function FSUSER_CreateFile(archive:FS_Archive; path:FS_Path; attributes:u32; fileSize:u64):s32;cdecl;external;

    function FSUSER_CreateDirectory(archive:FS_Archive; path:FS_Path; attributes:u32):s32;cdecl;external;

    function FSUSER_RenameDirectory(srcArchive:FS_Archive; srcPath:FS_Path; dstArchive:FS_Archive; dstPath:FS_Path):s32;cdecl;external;

    function FSUSER_OpenDirectory(out:PHandle; archive:FS_Archive; path:FS_Path):s32;cdecl;external;

    function FSUSER_OpenArchive(archive:PFS_Archive):s32;cdecl;external;

    function FSUSER_ControlArchive(archive:FS_Archive; action:FS_ArchiveAction; input:pointer; inputSize:u32; output:pointer; 
               outputSize:u32):s32;cdecl;external;

    function FSUSER_CloseArchive(archive:PFS_Archive):s32;cdecl;external;

    function FSUSER_GetFreeBytes(freeBytes:Pu64; archive:FS_Archive):s32;cdecl;external;

    function FSUSER_GetCardType(_type:PFS_CardType):s32;cdecl;external;

    function FSUSER_GetSdmcArchiveResource(archiveResource:PFS_ArchiveResource):s32;cdecl;external;

    function FSUSER_GetNandArchiveResource(archiveResource:PFS_ArchiveResource):s32;cdecl;external;

    function FSUSER_GetSdmcFatfsError(error:Pu32):s32;cdecl;external;

    function FSUSER_IsSdmcDetected(detected:Pbool):s32;cdecl;external;

    function FSUSER_IsSdmcWritable(writable:Pbool):s32;cdecl;external;

    function FSUSER_GetSdmcCid(out:Pu8; length:u32):s32;cdecl;external;

//the CID buffer Length should be 0x10
    function FSUSER_GetNandCid(out:Pu8; length:u32):s32;cdecl;external;

    function FSUSER_GetSdmcSpeedInfo(speedInfo:Pu32):s32;cdecl;external;

    function FSUSER_GetNandSpeedInfo(speedInfo:Pu32):s32;cdecl;external;

    function FSUSER_GetSdmcLog(out:Pu8; length:u32):s32;cdecl;external;

    function FSUSER_GetNandLog(out:Pu8; length:u32):s32;cdecl;external;

    function FSUSER_ClearSdmcLog:s32;cdecl;external;

    function FSUSER_ClearNandLog:s32;cdecl;external;

    function FSUSER_CardSlotIsInserted(inserted:Pbool):s32;cdecl;external;

    function FSUSER_CardSlotPowerOn(status:Pbool):s32;cdecl;external;

    function FSUSER_CardSlotPowerOff(status:Pbool):s32;cdecl;external;

    function FSUSER_CardSlotGetCardIFPowerStatus(status:Pbool):s32;cdecl;external;

    function FSUSER_CardNorDirectCommand(commandId:u8):s32;cdecl;external;

    function FSUSER_CardNorDirectCommandWithAddress(commandId:u8; address:u32):s32;cdecl;external;

    function FSUSER_CardNorDirectRead(commandId:u8; size:u32; output:Pu8):s32;cdecl;external;

    function FSUSER_CardNorDirectReadWithAddress(commandId:u8; address:u32; size:u32; output:Pu8):s32;cdecl;external;

    function FSUSER_CardNorDirectWrite(commandId:u8; size:u32; input:Pu8):s32;cdecl;external;

    function FSUSER_CardNorDirectWriteWithAddress(commandId:u8; address:u32; size:u32; input:Pu8):s32;cdecl;external;

    function FSUSER_CardNorDirectRead_4xIO(commandId:u8; address:u32; size:u32; output:Pu8):s32;cdecl;external;

    function FSUSER_CardNorDirectCpuWriteWithoutVerify(address:u32; size:u32; input:Pu8):s32;cdecl;external;

    function FSUSER_CardNorDirectSectorEraseWithoutVerify(address:u32):s32;cdecl;external;

    function FSUSER_GetProductInfo(info:PFS_ProductInfo; processId:u32):s32;cdecl;external;

    function FSUSER_GetProgramLaunchInfo(info:PFS_ProgramInfo; processId:u32):s32;cdecl;external;

    function FSUSER_SetCardSpiBaudRate(baudRate:FS_CardSpiBaudRate):s32;cdecl;external;

    function FSUSER_SetCardSpiBusMode(busMode:FS_CardSpiBusMode):s32;cdecl;external;

    function FSUSER_SendInitializeInfoTo9:s32;cdecl;external;

    function FSUSER_GetSpecialContentIndex(index:Pu16; mediaType:FS_MediaType; programId:u64; _type:FS_SpecialContentType):s32;cdecl;external;

    //legacy ROM header size = 0x3B4
    function FSUSER_GetLegacyRomHeader(mediaType:FS_MediaType; programId:u64; header:Pu8):s32;cdecl;external;

    //legacy banner data size = 0x23C0
    function FSUSER_GetLegacyBannerData(mediaType:FS_MediaType; programId:u64; banner:Pu8):s32;cdecl;external;

    function FSUSER_CheckAuthorityToAccessExtSaveData(access:Pbool; mediaType:FS_MediaType; saveId:u64; processId:u32):s32;cdecl;external;

    function FSUSER_QueryTotalQuotaSize(quotaSize:Pu64; directories:u32; files:u32; fileSizeCount:u32; fileSizes:Pu64):s32;cdecl;external;

    function FSUSER_AbnegateAccessRight(accessRight:u32):s32;cdecl;external;

    function FSUSER_DeleteSdmcRoot:s32;cdecl;external;

    function FSUSER_DeleteAllExtSaveDataOnNand:s32;cdecl;external;

    function FSUSER_InitializeCtrFileSystem:s32;cdecl;external;

    function FSUSER_CreateSeed:s32;cdecl;external;

    function FSUSER_GetFormatInfo(totalSize:Pu32; directories:Pu32; files:Pu32; duplicateData:Pbool; archiveId:FS_ArchiveID; 
               path:FS_Path):s32;cdecl;external;

    function FSUSER_GetLegacyRomHeader2(headerSize:u32; mediaType:FS_MediaType; programId:u64; header:Pu8):s32;cdecl;external;

    function FSUSER_GetSdmcCtrRootPath(out:Pu8; length:u32):s32;cdecl;external;

    function FSUSER_GetArchiveResource(archiveResource:PFS_ArchiveResource; mediaType:FS_MediaType):s32;cdecl;external;

    function FSUSER_ExportIntegrityVerificationSeed(seed:PFS_IntegrityVerificationSeed):s32;cdecl;external;

    function FSUSER_ImportIntegrityVerificationSeed(seed:PFS_IntegrityVerificationSeed):s32;cdecl;external;

    function FSUSER_FormatSaveData(archiveId:FS_ArchiveID; path:FS_Path; blocks:u32; directories:u32; files:u32; 
               directoryBuckets:u32; fileBuckets:u32; duplicateData:bool):s32;cdecl;external;

    function FSUSER_GetLegacySubBannerData(bannerSize:u32; mediaType:FS_MediaType; programId:u64; banner:Pu8):s32;cdecl;external;

    function FSUSER_ReadSpecialFile(bytesRead:Pu32; fileOffset:u64; size:u32; data:Pu8):s32;cdecl;external;

    function FSUSER_GetSpecialFileSize(fileSize:Pu64):s32;cdecl;external;

    function FSUSER_CreateExtSaveData(info:FS_ExtSaveDataInfo; directories:u32; files:u32; sizeLimit:u64; smdhSize:u32; 
               smdh:Pu8):s32;cdecl;external;

    function FSUSER_DeleteExtSaveData(info:FS_ExtSaveDataInfo):s32;cdecl;external;

    function FSUSER_ReadExtSaveDataIcon(bytesRead:Pu32; info:FS_ExtSaveDataInfo; smdhSize:u32; smdh:Pu8):s32;cdecl;external;

    function FSUSER_GetExtDataBlockSize(totalBlocks:Pu64; freeBlocks:Pu64; blockSize:Pu32; info:FS_ExtSaveDataInfo):s32;cdecl;external;

    function FSUSER_EnumerateExtSaveData(idsWritten:Pu32; idsSize:u32; mediaType:FS_MediaType; idSize:u32; shared:bool; 
               ids:Pu8):s32;cdecl;external;

    // blockSize of the save data. (usually 0x1000)
    function FSUSER_CreateSystemSaveData(info:FS_SystemSaveDataInfo; totalSize:u32; blockSize:u32; directories:u32; files:u32; 
               directoryBuckets:u32; fileBuckets:u32; duplicateData:bool):s32;cdecl;external;

    function FSUSER_DeleteSystemSaveData(info:FS_SystemSaveDataInfo):s32;cdecl;external;

    function FSUSER_StartDeviceMoveAsSource(context:PFS_DeviceMoveContext):s32;cdecl;external;

    function FSUSER_StartDeviceMoveAsDestination(context:FS_DeviceMoveContext; clear:bool):s32;cdecl;external;

    function FSUSER_SetArchivePriority(archive:FS_Archive; priority:u32):s32;cdecl;external;

    function FSUSER_GetArchivePriority(priority:Pu32; archive:FS_Archive):s32;cdecl;external;

    function FSUSER_SetCtrCardLatencyParameter(latency:u64; emulateEndurance:bool):s32;cdecl;external;

    function FSUSER_SwitchCleanupInvalidSaveData(enable:bool):s32;cdecl;external;

    function FSUSER_EnumerateSystemSaveData(idsWritten:Pu32; idsSize:u32; ids:Pu64):s32;cdecl;external;

    function FSUSER_InitializeWithSdkVersion(session:Handle; version:u32):s32;cdecl;external;

    function FSUSER_SetPriority(priority:u32):s32;cdecl;external;

    function FSUSER_GetPriority(priority:Pu32):s32;cdecl;external;

    function FSUSER_SetSaveDataSecureValue(value:u64; slot:FS_SecureValueSlot; titleUniqueId:u32; titleVariation:u8):s32;cdecl;external;

    function FSUSER_GetSaveDataSecureValue(exists:Pbool; value:Pu64; slot:FS_SecureValueSlot; titleUniqueId:u32; titleVariation:u8):s32;cdecl;external;

    function FSUSER_ControlSecureSave(action:FS_SecureSaveAction; input:pointer; inputSize:u32; output:pointer; outputSize:u32):s32;cdecl;external;

    function FSUSER_GetMediaType(mediaType:PFS_MediaType):s32;cdecl;external;

    function FSFILE_Control(handle:Handle; action:FS_FileAction; input:pointer; inputSize:u32; output:pointer; 
               outputSize:u32):s32;cdecl;external;

    function FSFILE_OpenSubFile(handle:Handle; subFile:PHandle; offset:u64; size:u64):s32;cdecl;external;

    function FSFILE_Read(handle:Handle; bytesRead:Pu32; offset:u64; buffer:pointer; size:u32):s32;cdecl;external;

    function FSFILE_Write(handle:Handle; bytesWritten:Pu32; offset:u64; buffer:pointer; size:u32; 
               flags:u32):s32;cdecl;external;

    function FSFILE_GetSize(handle:Handle; size:Pu64):s32;cdecl;external;

    function FSFILE_SetSize(handle:Handle; size:u64):s32;cdecl;external;

    function FSFILE_GetAttributes(handle:Handle; attributes:Pu32):s32;cdecl;external;

    function FSFILE_SetAttributes(handle:Handle; attributes:u32):s32;cdecl;external;

    function FSFILE_Close(handle:Handle):s32;cdecl;external;

    function FSFILE_Flush(handle:Handle):s32;cdecl;external;

    function FSFILE_SetPriority(handle:Handle; priority:u32):s32;cdecl;external;

    function FSFILE_GetPriority(handle:Handle; priority:Pu32):s32;cdecl;external;

    function FSFILE_OpenLinkFile(handle:Handle; linkFile:PHandle):s32;cdecl;external;

    function FSDIR_Control(handle:Handle; action:FS_DirectoryAction; input:pointer; inputSize:u32; output:pointer; 
               outputSize:u32):s32;cdecl;external;

    function FSDIR_Read(handle:Handle; entriesRead:Pu32; entryCount:u32; entries:PFS_DirectoryEntry):s32;cdecl;external;

    function FSDIR_Close(handle:Handle):s32;cdecl;external;

    function FSDIR_SetPriority(handle:Handle; priority:u32):s32;cdecl;external;

    function FSDIR_GetPriority(handle:Handle; priority:Pu32):s32;cdecl;external;

{$endif 3dsintf}

{$ifdef 3dsimpl}

    function mediaType(var a : FS_ProgramInfo) : FS_MediaType;
      begin
        mediaType:=(a.flag0 and bm_FS_ProgramInfo_mediaType) shr bp_FS_ProgramInfo_mediaType;
      end;

    procedure set_mediaType(var a : FS_ProgramInfo; __mediaType : FS_MediaType);
      begin
        a.flag0:=a.flag0 or ((__mediaType shl bp_FS_ProgramInfo_mediaType) and bm_FS_ProgramInfo_mediaType);
      end;

    function mediaType(var a : FS_ExtSaveDataInfo) : FS_MediaType;
      begin
        mediaType:=(a.flag0 and bm_PACKED_mediaType) shr bp_PACKED_mediaType;
      end;

    procedure set_mediaType(var a : FS_ExtSaveDataInfo; __mediaType : FS_MediaType);
      begin
        a.flag0:=a.flag0 or ((__mediaType shl bp_PACKED_mediaType) and bm_PACKED_mediaType);
      end;

    function mediaType(var a : FS_SystemSaveDataInfo) : FS_MediaType;
      begin
        mediaType:=(a.flag0 and bm_FS_SystemSaveDataInfo_mediaType) shr bp_FS_SystemSaveDataInfo_mediaType;
      end;

    procedure set_mediaType(var a : FS_SystemSaveDataInfo; __mediaType : FS_MediaType);
      begin
        a.flag0:=a.flag0 or ((__mediaType shl bp_FS_SystemSaveDataInfo_mediaType) and bm_FS_SystemSaveDataInfo_mediaType);
      end;

{$end 3dsimpl}

//for breifs & the origial C sources -->
//https://github.com/smealum/ctrulib/blob/master/libctru/include/3ds/services/fs.h
